﻿using ProductBacklog.Composition;
using ProductBacklog.Interfaces;
using ProductBacklog.ProductLoader.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reactive.Linq;
using System.Xml;

namespace ProductBacklog.ProductLoader.ViewModels
{
    [View("ProductCategoryView")]
    public class ProductCategoryViewModel : BaseViewModel, IProductCategory, ILoadable
    {
        private string selectedCategory;

        public ProductCategoryViewModel()
        {
            var query = from number in Enumerable.Range(1, 5) select number;
            var observableQuery = query.ToObservable();
            observableQuery.Subscribe(n => System.Diagnostics.Debug.WriteLine(n.ToString()), Imdone);
        }

        private void Imdone()
        {
            System.Diagnostics.Debug.WriteLine("Done");
        }

        public IEnumerable<string> CategoryList { get; private set; }

        public void OnLoaded()
        {
            this.CategoryList = new string[] { "VOD.L", "APPL", "GOOG.L", "BT", "BARC.L", "HSBA.L", "UBS", "C" };
            this.OnPropertyChanged(() => CategoryList);
        }

        public void OnUnloaded()
        {
        }

        public string SelectedCategory
        {
            get
            {
                return this.selectedCategory;
            }
            set
            {
                this.SetField(ref selectedCategory, value, () => SelectedCategory);
            }
        }
    }
}
